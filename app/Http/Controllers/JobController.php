<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\FetchMemberWithQueue;
use App\Member;

class JobController extends Controller
{
    public function work(Member $member, Request $request) {
        $this->dispatch(new FetchMemberWithQueue($request->all(), $member));
    }
}
