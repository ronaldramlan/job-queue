<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SendMemberViaEmail;
use App\Member;

class FetchMemberWithQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $input;
    protected $member;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input, Member $member)
    {
        $this->input = $input;
        $this->member = $member;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       \Mail::to('queue@queue.com')->send(new SendMemberViaEmail($this->input, $this->member));
    }
}
