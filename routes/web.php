<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Member;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});





// Job Queue Sending Email, Run Queue Worker from Controller
Route::group([
    'prefix' => 'queue',
], function() {

    Route::get('member/{member}', function(Request $request, Member $member) {
        App\Jobs\FetchMemberWithQueue::dispatch($request->all(), $member);
    });

    Route::get('member-controller/{member}', 'JobController@work');


});



